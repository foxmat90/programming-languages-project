Linguaggi di Programmazione

aa 2015-16



The project will consist of three parts, and each part should be delivered in a separate file. Each file should contain the complete solution of each point. 



Consider the function language which abstract syntax is specified in Ocaml as following:

   

  
```
#!ocaml

 type ide = string

    

   type exp = 

        Eint of int 

      | Ebool of bool 

      | Bigint of int list

      | Castint of exp

      | Emptylist

      | Cons of exp * exp

      | Head of exp

      | Tail of exp

      | Den of ide

      | Prod of exp * exp

      | Sum of exp * exp

      | Diff of exp * exp

      | Mod of exp * exp

      | Div of exp * exp

      | Less of exp * exp

      | Eq of exp * exp

      | Iszero of exp

      | Or of exp * exp

      | And of exp * exp

      | Not of exp

      | Pair of exp * exp

      | Fst of exp

      | Snd of exp

      | Ifthenelse of exp * exp * exp

      | Let of (ide * exp) list * exp      

      | Fun of ide list * exp

      | Apply of exp * exp list
```




The language has the following basic types:



small integer (Eint of int)

boolean (Ebool of bool)

functions (Fun of ide list * exp)

pairs (Pair of exp * exp)

unlimited precision integer (Bigint of int list) that we call big-integer

lists of any type (the constructors are Emptylist and Cons of exp * exp)



The language has the following features:



Blocks are delimited by the Let of (ide * exp) list * exp construct and (ide * exp) list are the local declarations of the block (for convenience they are a list)

Function can be parameters of other functions and can be the result of the evaluation of a program. 

The operations on integer and big-integers are sum, multiplication, division (it gives the quotient of the division of two numbers) and remainder (it gives the remainder  of the division of two numbers), and some relations are defined on integer and big-integer as well (less and equal). Furthermore there is an explicit cast from integers to big-integers (Castint of exp) that convert an integer into a big-integer. 

Pairs are constructed using Pair of exp * exp and elements are selected using Fst of exp and Snd of exp

Lists are constructed starting from Emptylist and Cons of exp * exp and the operations  Head of exp gives the head of the list (an element) and Tail if exp gives a list. A list is well formed only if all the elements of the list (beside Emptylist) have the same type. 



The main auxiliary functions must have the following names (the implementation of them is up to every group) and usage

emptyenv gives an empty environment (namely an environment where to all identifiers the value Unbound is associated)

bind takes an environment, an identifier and a value and give a new environment

applyenv takes an environment, an identifier and gives the value associated to the identifier in the environment

You may decide to treat special cases as you prefer, but you have to document the code.



For the project you have to 



write an interpreter (sem_static)  for the language above assuming that it is statically scoped. In particular you have to make the operations on expressions polymorphic in the sense that Prod of exp * exp,  Sum of exp * exp, Diff of exp * exp, Mod of exp * exp, Div of exp * exp should be able to treat integers and big-integers, with the convention that if an integer and a big-integer are involved then the result is a big-integer; Less of exp * exp should be polymorphic as well (returning a boolean) and the same for Eq of exp * exp and Iszero of exp. You have also the check that the list are well formed. Observe that the language has no recursion available.

10 points



As above but assuming that the language is dynamically scoped and you have to implement the deep binding policy when passing functional parameters. The interpreter should be called sem_dynamic.

10 points



Add to the language the exceptions, that are a guarded expressions with the following syntax: Try of exp * ide * exp where if an exception is raised while evaluating the first expression (and an exception is raised using the expression Raise of ide) and the identifier is the proper one, the the second expression is evaluated, otherwise the same exception is raised further. Extend the interpreter for the statically scoped version of the language.

10 points



      

       









Example of programs (all of them have to pass the test on the implementation of the dinamically scoped implementation). The first one, if the interpreter is properly implemented, should give a list of integer.




```
#!ocaml

Let([("y",Eint(1));

    ("f",Fun([],Cons(Den("y"),Emptylist)));

    ("g",Fun(["z"],Let([("y", Ebool(true))],Apply(Den("z"),[]))))],

    Apply(Den("g"),[Den("f")])

   ) 
```




This expression, when evaluated, should give the factorial of the integer “n”.




```
#!ocaml

Let([("fatt",

     Fun(["x"],

         Ifthenelse(Iszero(Den("x")),

                    Eint(1),

                    Prod(Den("x"), 

                         Apply(Den("fatt"),

                               [

                                Diff(Den("x"),

                                     Eint(1))

                               ])

                          )

                    )

          )

     )],

     Apply(Den("fatt"),[Eint(n)])

    )
```




        



The following will sort a list




```
#!ocaml

Let([("insert",

      Fun(["x";"list"],

         Ifthenelse(Eq(Den("list"),Emptylist),

                    Cons(Den("x"),Emptylist),

                    Ifthenelse(Less(Den("x"),Head(Den("list"))),

                               Cons(Den("x"),Den("list")), 

                        Cons(Head(Den("list")),Apply(Den("insert"),[Den("x");Tail(Den("list"))]))

                               )

                    )

         )

         );

      ("sort",

       Fun(["y"],

         Ifthenelse(Eq(Den("y"),Emptylist),

                    Emptylist,                 

                    Apply(Den("insert"),[Head(Den("y"));

                                    Apply(Den("sort"),

                                          [Tail(Den("y"))]

                                         )

                                    ]     

                            )

                    )

          )

       )

     ],

     Apply(Den("sort"),[Cons(Eint(3),Cons(Eint(4),Cons(Eint(1),Emptylist)))])

    )
```