(* Interprete sem_static con scope statico e try with *)
(* Gruppo id13. Componenti: Azzaro Edoardo, Garau Nicola, Lepori Massimiliano, Palla Mattia, Piras Cesare *)

(* Identificatori *)
type ide = string;;

(* Eccezione MyException *)
exception MyException of ide;;

(* Espressioni *)
type exp = 
    Eint of int 
  | Ebool of bool 
  | Bigint of int list
  | Castint of exp
  | Emptylist
  | Cons of exp * exp
  | Head of exp
  | Tail of exp
  | Den of ide
  | Prod of exp * exp
  | Sum of exp * exp
  | Diff of exp * exp
  | Mod of exp * exp
  | Div of exp * exp
  | Less of exp * exp
  | Eq of exp * exp
  | Iszero of exp
  | Or of exp * exp
  | And of exp * exp
  | Not of exp
  | Pair of exp * exp
  | Fst of exp
  | Snd of exp
  | Ifthenelse of exp * exp * exp
  | Let of (ide * exp) list * exp      
  | Fun of ide list * exp
  | Apply of exp * exp list
  | Try of exp * ide * exp
  | Raise of ide
  | Excn of exn;;

(* Tipi eval *)
type eval =
    Unbound
  | Evint of int
  | Evbool of bool
  | Evbigint of int list
  | Evlist of eval list
  | Evcons of exp*exp
  | Evpair of exp * exp
  | Evfun of ide list * exp * env
  | Evtry of exp * ide * exp
  | Evraise of ide
  | Evexcn of exn

 (* Dichiarazione ambiente *)		   
 and env = Env of (ide -> eval);;

(* Eccezione Identificatore Unbound *)
exception UnboundIde of ide;;

(* Funzione che crea un ambiente vuoto *)
let emptyenv= fun()-> Env(fun x -> Unbound);;

(* Funzione per l'associazione identificatori-valori, restituisce un ambiente *)
let bind (Env rho) x d = Env (fun y -> if y=x then d else rho y);;

(* Restituisce il valore di una variabile in un ambiente *)
let applyenv (Env rho) x = match rho x with
    Unbound -> raise (UnboundIde x)
  | _ as d -> d;;

(* Controllo dei tipi *)
let typecheck (variabile, tipo) = match tipo with
    "intero"   -> (match variabile with Evint(i) -> true |_ -> false)
  | "booleano" -> (match variabile with Evbool(b) -> true |_ -> false)
  | "bigint"   -> (match variabile with Evbigint(bi) -> true |_ -> false)
  | "emptylist"-> (match variabile with Evlist([])-> true |_ -> false)     
  | "lista"    -> (match variabile with Evlist(l) -> true |_ -> false)
  | "coppia"   -> (match variabile with Evpair(l1, l2) -> true |_ -> false)
  | _          -> failwith("Tipo non riconosciuto");;

(* Restituisce il tipo di una variabile *)
let returnType variabile = match variabile with
    Evint(i) -> "intero"
  | Evbool(b) -> "booleano"
  | Evbigint(bi) ->"bigint"
  | Evlist([])-> "emptylist"     
  | Evlist(l) -> "lista"
  | Evpair(l1, l2) -> "coppia"
  | _          -> failwith("Tipo non riconosciuto");;

(* Restituisce true se negativo e false altrimenti *)
let isNegative (n) = if( typecheck(n,"intero") ) then (
		       match n with
			 Evint(num) -> if(num<0) then true else false
		     )
		     else (if(typecheck(n,"bigint")) then (
			     match n with
			       Evbigint(hd::tl) -> if(hd<0) then true else false
			   )
			   else raise (MyException ("Operandi non interi")));;

(* Calcolo del valore assoluto, restituisce un tipo eval *)
let absoluteValue (n) = if( typecheck(n,"intero") ) then (
			  match n with
			    Evint(num) -> Evint(abs(num))
			)
			else (if(typecheck(n,"bigint")) then (
				match n with
				  Evbigint(hd::tl) -> Evbigint(abs(hd)::tl)
			      )
			      else raise (MyException ("Operandi non interi")));;

(* Calcolo del valore assoluto, restituisce una lista *)
let absoluteValueB (n) = if(typecheck(n,"bigint")) then (
			   match n with
			     Evbigint(hd::tl) -> (abs(hd)::tl))
			 else raise (MyException ("Operando non bigint"));;

(* Funzione ausiliaria per il cast *)
let rec castTemp (n) = match n with
    Evint(n1) when n1<(-9)  -> castTemp(Evint(n1/10))@[abs(n1 mod 10)]
  | Evint(n1) when n1<0  -> [n1]
  | Evint(n1) when n1<10 -> [n1]
  | Evint(n1)            -> castTemp(Evint(n1/10))@[n1 mod 10]
  | _                    -> raise (MyException ("Errore match cast"));;

(* Serve per il cast da int a lista *)
let rec cast (n) = if( typecheck(n,"intero")) then
		     (castTemp(n))
		   else raise (MyException("Parametri del cast non corretti"));;

(* Serve per il cast da int a Bigint *)
let rec castEv (n) = if( typecheck(n,"intero")) then
		       Evbigint(castTemp(n))
		     else Evexcn (MyException("Parametri del cast non corretti"));;

(* Confronto tra due interi/bigint, restituisce un booleano *)
let lesser (n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero")) then n1<n2
		     else
		       (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			then match (n1,n2) with 
			       Evbigint(np),Evbigint(ns) -> if (List.length(np)<List.length(ns)) then true 
							    else (if ((List.length np) > (List.length ns) ) then false else np<ns) 
			else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			      then n1<Evbigint(cast(n2))
			      else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			      then Evbigint(cast(n1))<n2
			      else raise (MyException ("Operandi non interi"))));;

(* Confronto tra due interi/bigint, restituisce un eval *)
let lesserEv (n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero")) then Evbool(n1<n2)
		       else
			 (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			  then (match (n1,n2) with 
				  Evbigint(np),Evbigint(ns) -> if (List.length(np)<List.length(ns)) then Evbool(true)
							       else (if ((List.length np) > (List.length ns) ) then Evbool(false) else Evbool(np<ns))) 
			  else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
				then Evbool(n1<Evbigint(cast(n2)))
				else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
				then Evbool(Evbigint(cast(n1))<n2)
				else Evexcn (MyException ("Operandi non interi"))));;

(* Crea una lista a partire da una coppia *)
let createlist (e1,e2) =
  match e1,e2 with
    Evint(p),Evint(s)           -> Evlist ([Evint(p)]@[Evint(s)])
  | Evbigint(p),Evbigint(s)     -> Evlist ([Evbigint(p)]@[Evbigint(s)])
  | Evbool(p),Evbool(s)         -> Evlist ([Evbool(p)]@[Evbool(s)])
  | Evlist(p),Evint(s)          -> if p = [] || typecheck (List.hd p,"intero")
                                   then Evlist (e2::p) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evlist(p),Evbigint(s)       -> if p = [] || typecheck (List.hd p,"bigint")
                                   then Evlist (e2::p) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evlist(p),Evbool(s)         -> if p = [] || typecheck (List.hd p,"booleano")
                                   then Evlist (e2::p) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evlist(p),Evpair(s,z)       -> if p = [] || typecheck (List.hd p,"coppia")
                                   then Evlist (e2::p) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evlist(p),Evlist(s)         -> if p = [] || s = [] || returnType (List.hd p)=returnType (List.hd s)
                                   then Evlist (p @ s) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evint(p),Evlist(s)          -> if s = [] || typecheck (List.hd s,"intero")
                                   then Evlist (e1::s) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evbigint(p),Evlist(s)       -> if s = [] || typecheck (List.hd s,"bigint")
                                   then Evlist (e1::s) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evbool(p),Evlist(s)         -> if s = [] || typecheck (List.hd s,"booleano")
                                   then Evlist (e1::s) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi"))
  | Evpair(p,z),Evlist(s)       -> if s = [] || typecheck (List.hd s,"coppia")
                                   then Evlist (e1::s) else Evexcn (MyException ("Errore. Sono stati inseriti tipi diversi")) 
  | Evpair(p1,p2),Evpair(s1,s2) -> Evlist ([Evpair(p1,p2)]@[Evpair(s1,s2)])
  | _ -> Evexcn (MyException ("Riscontrati tipi diversi nella lista."))
;;


(* Restituisce la testa di una lista *)
let gethead(e) = if( typecheck(e,"lista") ) then match e with
						 | Evlist(l) -> List.hd l
		 else Evexcn (MyException ("Il parametro non è una lista"));;

(* Restituisce la coda di una lista *)
let gettail(e) = if( typecheck(e,"lista") ) then match e with
						 | Evlist(l) -> Evlist(List.tl l)
		 else Evexcn (MyException ("Il parametro non è una lista"));;

(* Toglie gli zeri a sinistra di un numero (codificato come lista, es [1;2;3] vale 123) *)
let rec toglizeri(l) = match l with
    [] -> [0]
  | hd::tl -> if(hd=0) then toglizeri(tl) else l;;
  
(* Inverte il segno di un numero (codificato come lista, es [1;2;3] vale 123) *)
let cambiaSegno(l) = match l with
    hd::tl -> (hd*(-1))::tl
  | _ -> raise (MyException("Errore cambiaSegno"));;		      			      

(* Esegue la sottrazione di due numeri codificati come lista *)
let sottrailiste (l1,l2) = 
  let rec sottrailisteProvaR (l1,l2,risultato, prestito) = match (l1,l2)with
      (hd1::[]), [] when (prestito = 0) -> (hd1-0)::risultato
    | (hd1::[]), [] when (prestito = 1) -> (hd1-1)::risultato
    | (l1,[])  when (prestito = 0)      -> List.rev(l1)@risultato
    | (l1,[])  when (prestito = 1)      -> sottrailisteProvaR(l1,[1],[],0)@risultato
    | (hd1::tl1, hd2::tl2) when (hd1-hd2<0 && prestito = 0)           -> 
       sottrailisteProvaR (tl1,tl2,[hd1-hd2+10]@risultato,1)
    | (hd1::tl1, hd2::tl2) when (hd1-hd2-prestito<0 && prestito=1)    -> 
       sottrailisteProvaR (tl1,tl2,[hd1-hd2+9]@risultato,1)
    | (hd1::tl1, hd2::tl2) when (hd1-hd2>=0 && prestito = 0)          -> 
       sottrailisteProvaR((tl1,tl2,[hd1-hd2]@risultato, 0)) 	 
    | (hd1::tl1, hd2::tl2) when (hd1-hd2-prestito>=0 && prestito = 1) -> 
       sottrailisteProvaR((tl1,tl2,[hd1-hd2-prestito]@risultato, 0))
  in toglizeri(sottrailisteProvaR(List.rev(l1),List.rev(l2),[], 0));;
  
(* Esegue la somma di due numeri codificati come lista *)
let sommaliste(l1,l2) =
  let rec sommalisteR(l1,l2,riporto) = match (l1,l2) with
      ([],[]) -> (riporto::[])
    | (hd1::tl1, []) -> (sommalisteR(l1,[riporto],0))
    | ([], hd2::tl2) -> (sommalisteR(l2,[riporto],0))
    | (hd1::tl1, hd2::tl2) -> (match(hd1+hd2+riporto) with
				 n when n<10 -> (sommalisteR(tl1,tl2,0))@[hd1+hd2+riporto]
			       | _           -> (sommalisteR(tl1,tl2,1))@[(hd1+hd2+riporto) mod 10])
  in toglizeri(sommalisteR(List.rev l1, List.rev l2,0));;

(* Gestisce il segno nella sottrazione tra due liste (richiama sottraiListe per l'operazione base) *)	
let sottraiSegnoListe(l1,l2) = match (l1,l2) with
    (a,b) when (not (isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
    -> if(lesser(absoluteValue(Evbigint(l2)), absoluteValue(Evbigint(l1))))
       then sottrailiste(l1,l2)
       else cambiaSegno(sottrailiste(l2,l1)) (*CAMBIARE SEGNO*)
  | (a,b) when (not (isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
    -> sommaliste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
     -> cambiaSegno(sommaliste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))) (*CAMBIARE SEGNO*) 
  |  (a,b) when ((isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
     -> if(lesser(absoluteValue(Evbigint(l2)), absoluteValue(Evbigint(l1))))
	then cambiaSegno(sottrailiste(absoluteValueB(Evbigint(l1)),absoluteValueB(Evbigint(l2)))) (*CAMBIARE SEGNO*)
	else sottrailiste(absoluteValueB(Evbigint(l2)),absoluteValueB(Evbigint(l1)))
  | _ -> failwith "Caso inesistente";;

(* Gestisce il controllo dei tipi nella sottrazione, richiama sottraiSegnoListe *)
let sottrai(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		     then match(n1,n2) with
			    (Evint(p),Evint(s)) -> Evint(p-s)
		     else
		       (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			then match(n1,n2) with
			       (Evbigint(p),Evbigint(s)) ->
			       (
				 match (p,s) with
				   (l1,l2) -> Evbigint(sottraiSegnoListe(l1,l2))
			       (* (l1,l2) when List.length l1 = List.length l2 && l1>l2 ->  Evbigint(sottrailiste(l1,l2)) *)
			       (*   | (l1,l2) when List.length l1 = List.length l2 -> Evbigint(sottrailiste(l2,l1)) (\* aggiungere meno *\) *)
			       (*   | (l1,l2) when List.length l1 > List.length l2 -> Evbigint(sottrailiste(l1,l2)) *)
			       (*   | (l1,l2) -> Evbigint(sottrailiste(l2,l1)) (\* aggiungere meno *\) *)
			       )
			else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			      then match(n1) with
				     (Evbigint(p)) -> Evbigint(sottraiSegnoListe(p,castTemp(n2)))
			      else if(typecheck(n1,"intero") && typecheck(n2,"bigint")) (* aggiungere meno *)
			      then match(n2) with
				     (Evbigint(s)) -> Evbigint(sottraiSegnoListe(castTemp(n1),s))
			      else Evexcn (MyException ("Operandi non interi"))));;

(* Gestisce il segno nella somma tra due liste (richiama sommaliste per l'operazione base) *)
let sommaSegno(l1,l2) = match (l1,l2) with
    (a,b) when (not (isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
    -> sommaliste(l1 , l2)
  | (a,b) when (not (isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
    -> sottraiSegnoListe(l1 ,absoluteValueB(Evbigint(l2)))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && (not (isNegative(Evbigint(l2))))
     -> cambiaSegno(sottraiSegnoListe(absoluteValueB(Evbigint(l1)) , l2))
  |  (a,b) when ((isNegative(Evbigint(l1)))) && ((isNegative(Evbigint(l2))))
     -> cambiaSegno(sommaliste(absoluteValueB(Evbigint(l1)) , absoluteValueB(Evbigint(l2))))
  | _ -> raise (MyException ("Caso inesistente"));;

(* Gestisce il controllo dei tipi nella somma, richiama sommaSegno *)
let somma(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		   then match(n1,n2) with
			  (Evint(p),Evint(s)) -> Evint(p+s)
		   else
		     (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
		      then match(n1,n2) with
			     (Evbigint(p),Evbigint(s)) -> Evbigint(sommaSegno(p,s))
		      else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			    then match(n1) with
				   (Evbigint(p)) -> Evbigint(sommaSegno(p,castTemp(n2)))
			    else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			    then match(n2) with
				   (Evbigint(s)) -> Evbigint(sommaSegno(castTemp(n1),s))
			    else Evexcn (MyException ("Operandi non interi"))));;

(* Funzione ausiliaria di moltiplicaArray, gestisce il riporto nella moltiplicazione *)
let moltiplicaMap(le,n) = match (le*n) with
    tot when tot<10 -> [tot]
  | tot             -> [(tot)/10; (tot) mod 10];;

(* Funzione ausiliaria di moltiplicaliste, moltiplica una cifra per un numero codificato come lista *)
let moltiplicaArray(l,n)= 
  let rec moltiplicaArrayR(l,n,risultato,zeri) = match l with
      []     -> risultato
    | hd::tl -> moltiplicaArrayR(tl, n, (sommaliste(risultato,(moltiplicaMap(hd,n))@zeri)), zeri@[0])
  in moltiplicaArrayR(List.rev l,n,[],[]);;

(* Esegue la moltiplicazione tra due numeri codificati come liste *)
let moltiplicaliste(l1,l2) =
  if(l1=[] || l2=[]) then raise (MyException ("Parametri non validi")) else
    (
      let rec moltiplicalisteR(l1,l2,risParz,zeri) =
	match l2 with
	  []     -> risParz
	| hd::tl -> moltiplicalisteR(l1,tl,sommaliste(risParz,moltiplicaArray(l1,hd)@zeri),zeri@[0])
      in moltiplicalisteR(l1, List.rev l2, [], [])
    );;

(* Gestisce il controllo dei tipi nella moltiplicazione, richiama moltiplicaliste *)
let moltiplica(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
			then match(n1,n2) with
			       (Evint(p),Evint(s)) -> Evint(p*s)
			else
			  (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			   then match(n1,n2) with
				  (Evbigint(p),Evbigint(s))
				  -> if((isNegative(n1) && (not(isNegative(n2))))
					|| (not (isNegative(n1)) && isNegative(n2)))
				     then Evbigint(cambiaSegno(moltiplicaliste(absoluteValueB(n1),absoluteValueB(n2))))
				     else Evbigint(moltiplicaliste(absoluteValueB(n1),absoluteValueB(n2)))
			   else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
				 then (
				   match(n1) with
				     (Evbigint(p))
				     -> if((isNegative(n1) && (not(isNegative(n2))))
					   || (not (isNegative(n1)) && isNegative(n2)))
					then Evbigint(cambiaSegno(moltiplicaliste(absoluteValueB(n1),castTemp(absoluteValue(n2)))))
					else Evbigint(moltiplicaliste(absoluteValueB(n1),castTemp(absoluteValue(n2))))
				 )
				 else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
				 then (
				   match(n2) with
				     (Evbigint(s))
				     -> if((isNegative(n1) && (not(isNegative(n2))))
					   || (not (isNegative(n1)) && isNegative(n2)))
					then Evbigint(cambiaSegno(moltiplicaliste(castTemp(absoluteValue(n1)),absoluteValueB(n2))))
					else Evbigint(moltiplicaliste(castTemp(absoluteValue(n1)),absoluteValueB(n2)))
				 )
				 else Evexcn (MyException ("Operandi non interi"))));;



  

(* Esegue la divisione tra due numeri codificati come liste *)
let dividiliste(l1,l2) =
  let rec dividilisteR(l1, l2, risultato) = match (l1,l2) with
      ([],[]) -> raise (MyException ("Parametri vuoti"))
    | ([],l2) -> raise (MyException ("Dividendo vuoto"))
    | (l1,[]) -> raise (MyException ("Errore dividiliste"))
    | (l1,l2) -> if(List.length l1 < List.length l2
		    || (List.length l1 = List.length l2) && l1<l2)
		 then risultato
		 else
		   (
		     if((List.length l1 = List.length l2) && l1=l2)
		     then sommaliste(risultato, [1])
		     else dividilisteR(sottrailiste(l1,l2), l2, sommaliste(risultato, [1]))
		   )
  in dividilisteR(l1, l2, [0]);;

(* Gestisce il controllo dei tipi nella divisione, richiama dividiliste *)
let dividi(n1,n2) = if( typecheck(n1,"intero") && typecheck(n2,"intero"))
		    then match(n1,n2) with
			   (Evint(p),Evint(s)) -> Evint(p/s)
		    else
		      (if( typecheck(n1,"bigint") && typecheck(n2,"bigint"))
		       then match(n1,n2) with
			      (Evbigint(p),Evbigint(s))
			      -> if((isNegative(n1) && (not(isNegative(n2))))
				    || (not (isNegative(n1)) && isNegative(n2)))
				 then Evbigint(cambiaSegno(dividiliste(absoluteValueB(n1),absoluteValueB(n2))))
				 else Evbigint(dividiliste(absoluteValueB(n1),absoluteValueB(n2)))
		       else (if(typecheck(n1,"bigint") && typecheck(n2,"intero"))
			     then (
			       match(n1) with
				 (Evbigint(p))
				 -> if((isNegative(n1) && (not(isNegative(n2))))
				       || (not (isNegative(n1)) && isNegative(n2)))
				    then Evbigint(cambiaSegno(dividiliste(absoluteValueB(n1),castTemp(absoluteValue(n2)))))
				    else Evbigint(dividiliste(absoluteValueB(n1),castTemp(absoluteValue(n2))))
			     )
			     else if(typecheck(n1,"intero") && typecheck(n2,"bigint"))
			     then (
			       match(n2) with
				 (Evbigint(s))
				 -> if((isNegative(n1) && (not(isNegative(n2))))
				       || (not (isNegative(n1)) && isNegative(n2)))
				    then Evbigint(cambiaSegno(dividiliste(castTemp(absoluteValue(n1)),absoluteValueB(n2))))
				    else Evbigint(dividiliste(castTemp(absoluteValue(n1)),absoluteValueB(n2)))
			     )
			     else Evexcn (MyException ("Operandi non interi"))));;

(* Controlla se due variabili sono equivalenti, restituisce un booleano *)
let equals (n1,n2) = if((typecheck(n1,"intero") && typecheck(n2,"intero"))
			|| (typecheck(n1,"booleano") && typecheck(n2,"booleano"))
			|| (typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			|| (typecheck(n1,"lista") && typecheck(n2,"lista"))) then n1=n2
		     else ( if(typecheck(n1,"intero") && typecheck(n2,"bigint"))then (Evbigint(cast(n1))=n2) 
			    else (if (typecheck(n1,"bigint") && typecheck(n2,"intero")) then (n1=Evbigint(cast(n2)))
				  else raise (MyException "Operandi non corretti")));;

(* Controlla se due variabili sono equivalenti, restituisce un eval *)
let equalsEv (n1,n2) = if((typecheck(n1,"intero") && typecheck(n2,"intero"))
			  || (typecheck(n1,"booleano") && typecheck(n2,"booleano"))
			  || (typecheck(n1,"bigint") && typecheck(n2,"bigint"))
			  || (typecheck(n1,"lista") && typecheck(n2,"lista"))) then Evbool(n1=n2)
		       else ( if(typecheck(n1,"intero") && typecheck(n2,"bigint"))then Evbool(Evbigint(cast(n1))=n2) 
			      else (if (typecheck(n1,"bigint") && typecheck(n2,"intero")) then Evbool(n1=Evbigint(cast(n2)))
				    else Evexcn (MyException "Operandi non corretti")));;

(* Controlla se un numero è equivalente a zero, restituisce un booleano *)
let zero (n) = if( typecheck(n,"intero") ) then n=Evint(0)
	       else (if(typecheck(n,"bigint")) then n=Evbigint([0])
		     else raise (MyException ("Operandi non interi")));;

(* Controlla se un numero è equivalente a zero, restituisce un eval *)
let zeroEv (n) = if( typecheck(n,"intero") ) then Evbool(n=Evint(0))
		 else (if(typecheck(n,"bigint")) then Evbool(n=Evbigint([0]))
		       else Evexcn (MyException ("Operandi non interi")));;

(* Or logico tra due booleani *)
let orTemp (b1,b2) = if( typecheck(b1,"booleano") && typecheck(b2,"booleano")) then
		       (match (b1,b2) with
     			| Evbool(b1), Evbool(b2) -> Evbool(b1||b2) )
		     else Evexcn (MyException ("Operandi non booleani"));;

(* And logico tra due booleani *)
let andTemp (b1,b2) = if( typecheck(b1,"booleano") && typecheck(b2,"booleano")) then
			(match (b1,b2) with
     			 | Evbool(b1), Evbool(b2) -> Evbool(b1&&b2) )
		      else Evexcn (MyException ("Operandi non booleani"));;

(* Negazione (Not) di un booleano *)
let negation (b) = if(typecheck(b,"booleano")) then (match b with
     						     | Evbool(b1) -> Evbool(not b1) )
		   else Evexcn (MyException ("Operando non booleano"));;

(* Restituisce il primo elemento di una coppia, gestisce anche coppie di liste e coppie di coppie *)
let getFirst (p) = if(typecheck(p,"coppia"))
		   then (match p with
			   Evpair(primo,secondo) -> (match primo with
						       Eint(p) -> Evint(p)
						     | Ebool(p) -> Evbool(p)
						     | Bigint(p) -> Evbigint(p)
						     | Emptylist -> Evlist([])
						     | Cons(p1,p2) -> Evcons(p1,p2)
						     | Pair(p1,p2) -> Evpair(p1,p2)
						     | _ -> Evexcn (MyException("Errore match getFirst")))
			  |_ -> Evexcn (MyException("Errore match getFirst (esterno)")))
		   else Evexcn (MyException ("Operando non coppia"));;

(* Restituisce il secondo elemento di una coppia, gestisce anche coppie di liste e coppie di coppie *)
let getSecond (p) = if(typecheck(p,"coppia"))
		    then (match p with
			    Evpair(primo,secondo) -> (match secondo with
							Eint(p) -> Evint(p)
						      | Ebool(p) -> Evbool(p)
						      | Bigint(p) -> Evbigint(p)
						      | Emptylist -> Evlist([])
						      | Cons(p1,p2) -> Evcons(p1,p2)
						      | Pair(p1,p2) -> Evpair(p1,p2)
						      | _ -> Evexcn (MyException("Errore match getSecond")))
			   |_ -> Evexcn (MyException("Errore match getSecond (esterno)")))
		    else Evexcn (MyException ("Operando non coppia"));;

(* Controlla i tipi, funzione d'appoggio per l'ifthenelse *)
let evaluateif(c,e1,e2) = if(typecheck(c,"booleano")) then
			    (match c with
			       Evbool(b) -> if(b) then e1 else e2 
			     | _ -> raise (MyException ("Errore match evaluateif")))
			  else raise (MyException ("Operando non booleano o espressioni di tipo diverso"));;

(* Non la usiamo *)
(* let rec delimita (iel,e,rho) = match iel with *)
(*     [] -> e *)
(*   | hd::tl -> delimita(tl,e,(bind rho (fst hd) (snd hd)));; *)

  
(* Interprete con scope statico e try with *)
let rec sem_static exp rho = match exp with
    Eint(i) -> Evint(i)			 (* OK *)
  | Ebool(b) -> Evbool(b)		 (* OK *)
  | Bigint(il) -> Evbigint(il)		 (* OK *)
  | Castint(i) -> castEv(sem_static i rho) (* OK *)
  | Emptylist -> Evlist([])		 (* OK *)
  | Cons(e1,e2) -> createlist(sem_static(e1) rho, sem_static(e2) rho) (* OK *)
  | Head(e) -> gethead(sem_static e rho) (* OK *)
  | Tail(e) -> gettail(sem_static e rho) (* OK *)
  | Den(i) -> applyenv (rho) i		 (* OK *)
  | Prod (e1,e2) -> moltiplica((sem_static e1 rho), (sem_static e2 rho)) (* OK *)
  | Sum (e1,e2) -> somma((sem_static e1 rho), (sem_static e2 rho))       (* OK *)
  | Diff (e1,e2) -> sottrai((sem_static e1 rho), (sem_static e2 rho))    (* OK *)
  | Mod (e1,e2) -> sottrai(sem_static e1 rho ,moltiplica(sem_static e2 rho, dividi((sem_static e1 rho), (sem_static e2 rho)))) (* OK *)
  | Div (e1,e2) -> dividi((sem_static e1 rho), (sem_static e2 rho))      (* OK *)
  | Less (e1,e2) -> lesserEv((sem_static e1 rho), (sem_static e2 rho))   (* OK *)
  | Eq (e1,e2) -> equalsEv((sem_static e1 rho), (sem_static e2 rho))     (* OK *)
  | Iszero (e) -> zeroEv(sem_static e rho)                               (* OK *)
  | Or (e1,e2) -> orTemp((sem_static e1 rho), (sem_static e2 rho))       (* OK *)
  | And (e1,e2) -> andTemp((sem_static e1 rho), (sem_static e2 rho))     (* OK *)
  | Not (e) -> (negation(sem_static e rho))                              (* OK *)
  | Pair(e1,e2) -> Evpair(e1,e2)	                                 (* OK *)
  | Fst(e) -> getFirst(sem_static e rho)                                 (* OK *)
  | Snd(e) -> getSecond(sem_static e rho)                                (* OK *)
  | Ifthenelse(cond,e1,e2) ->  let ite = sem_static cond rho in
                               if typecheck(ite,"booleano")
			       then
                                 (if ite = Evbool(true)
                                  then sem_static e1 rho
                                  else sem_static e2 rho)
                               else failwith ("Condizione non booleana (Ifthenelse)")
  | Let(il,e) -> (
    match(il) with
      [] -> sem_static e rho
    | hd::tl -> sem_static (Let (tl,e)) (bind (rho) (fst hd) (sem_static (snd hd) rho))
  )
  | Fun(il,e) -> Evfun(il,e,rho)
  | Apply(e,el) -> (match ((sem_static (e) rho),el) with
		      ( (Evfun(pf,body,rho)) , pa  ) -> sem_static body (applyTemp pf pa rho)
		    | _ -> Evexcn(MyException( "Errore parametri")))
  | Try(e1,i,e2) -> if(sem_static(e1) rho=Evexcn(MyException(i)))
		    then sem_static(Raise(i)) rho
  		    else sem_static(e2) rho
  | Raise(i) -> Evexcn(MyException(i))
  | Excn(e) -> Evexcn(e)

(* Funzione ausiliaria per la Apply, richiama sem_static *)
and applyTemp pf pa rhoTemp = match (pf,pa) with 
    ([],_) -> rhoTemp
  | (hd::tl,[]) -> applyTemp tl [] (bind (rhoTemp) (hd) Unbound)
  | (hd1::tl1,hd2::tl2) -> applyTemp tl1 tl2 (bind (rhoTemp) (hd1) (sem_static (hd2) rhoTemp));;
